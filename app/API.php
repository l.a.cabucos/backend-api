<?php
    require_once('MysqliDb.php');
    error_reporting(E_ALL);
    ini_set('display_errors', 1);

    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Credentials: true");
    header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");
    header("Access-Control-Allow-Headers: Content-Type");

    class API
    {
        private $db;

        public function __construct()
        {
            $this->db = new MysqliDb('localhost', 'root', '', 'employee');
        }
        protected function successResponse($method, $status, $data): array
        {
            header("Content-Type: application/json");
            http_response_code(200);
            return [
                'method' => $method,
                'status' => $status,
                'data' => $data,
            ];
        }

        protected function failedResponse($method, $status, string $message): array
        {
            header("Content-Type: application/json");
            http_response_code(400);
            return [
                'method' => $method,
                'status' => $status,
                'message' => $message,
            ];
        }

        public function httpGet($payload): bool
        {
            if (!is_array($payload)) {
                $response = $this->failedResponse('GET', 'failed', 'Invalid payload. Payload must be an array');
                echo json_encode($response);
                return 0;
            }
        
            if (isset($payload['id']) || isset($payload['first_name'])) {
                // Retrieves employee details by using id
                if (isset($payload['id'])) {
                    $this->db->where('id', $payload['id']);
                    $result = $this->db->getOne('information'); 
        
                    if ($result) {
                        $response = $this->successResponse('GET', 'success', $result);
                        echo json_encode($response);
                        return 1;
                    } else {
                        $response = $this->failedResponse('GET', 'failed', 'Failed to Retrieve Employee Data');
                        echo json_encode($response);
                        return 0;
                    }
                }
                // Retrieves employee details by using first_name
                if (isset($payload['first_name'])) {
                    $this->db->where('first_name', $payload['first_name']);
                    $result = $this->db->getOne('information');
        
                    if ($result) {
                        $response = $this->successResponse('GET', 'success', $result);
                        echo json_encode($response);
                    } else {
                        $response = $this->failedResponse('GET', 'failed', 'Failed to Retrieve Employee Data');
                        echo json_encode($response);
                    }
                }
            } elseif (empty($payload)) {
                // Retrieves all data if payload is empty
                $result = $this->db->get('information');
                if ($result) {
                    $response = $this->successResponse('GET', 'success', $result);
                    echo json_encode($response);
                } else {
                    $response = $this->failedResponse('GET', 'failed', 'Failed to Retrieve Employee Data');
                    echo json_encode($response);
                }
        
                return 1;
            }
        
            return 0;
        }
        
        
        public function httpPost($payload): bool
        {
            // The required parameters for POST based on Onboarding API Documentation
            $requiredParams = ['first_name', 'last_name'];
        
            // Check if any required parameter is missing in the payload
            $missingParams = array_diff($requiredParams, array_keys($payload));
            if (!empty($missingParams)) {
                $response = $this->failedResponse('POST', 'failed', 'Required Fields not Filled: ' . implode(', ', $missingParams));
                echo json_encode($response);
                return 0;
            }
        
            $result = $this->db->insert('information', $payload);
        
            if ($result) {
                $response = $this->successResponse('POST', 'success', $result);
            } else {
                $response = $this->failedResponse('POST', 'failed', 'Failed to Insert Employee Data');
            }
        
            echo json_encode($response);
            return $result !== 0;
        }
        

        public function httpPut($id, $payload): bool
        {
            $flag = 0;

            // The required parameters for PUT based on Onboarding API Documentation
            $requiredParams = ['id', 'first_name', 'last_name'];

            // Check if any required parameter is missing in the payload
            $missingParams = array_diff($requiredParams, array_keys($payload));
            if (!empty($missingParams)) {
                $response = $this->failedResponse('PUT', 'failed', 'Required Fields not Filled: ' . implode(', ', $missingParams));
                echo json_encode($response);
                return false;
            }

            if (!is_null($id) && !empty($id)) {
                // Ensure $payload is an array and not empty
                if (is_array($payload) && !empty($payload)) {
                    // Ensure that the 'id' field is present in the payload and matches the $id
                    if (isset($payload['id']) && $id == $payload['id']) {
                        $this->db->where('id', $id);
            
                        // Execute the query using MysqliDb class to update employee data
                        $result = $this->db->update('information', $payload);
            
                        if ($result) {
                            $response = $this->successResponse('PUT', 'success', $result);
                            echo json_encode($response);
                        } else {
                            $response = $this->failedResponse('PUT', 'failed', 'Failed to Update Employee Data');
                            echo json_encode($response);
                        }
                    } else {
                        $response = $this->failedResponse('PUT', 'failed', 'ID mismatch between URL and payload');
                        echo json_encode($response);
                    }
                } else {
                    $response = $this->failedResponse('PUT', 'failed', 'ID or payload is null or not an array');
                    echo json_encode($response);
                }
            }
            

            return $flag !== 0;
        }

        public function httpDelete($id, $payload): bool {
            $flag = 0; 
        
            // Ensure if $id is not null
            if (!is_null($id) && !empty($id)) {
                // Ensure if $payload is an array and if not null
                if (is_array($payload) && !empty($payload['id'])) {
                    // Ensure that the 'id' field is present in the payload and matches the $id
                    if (isset($payload['id']) && $id == $payload['id']) {
                        // Ensure if $payload is an array or not
                        if (is_array($payload)) {
                            $result = $this->db->where('id', $payload, 'IN')->delete('information');
                        } else {
                            $result = $this->db->where('id', $payload)->delete('information');
                        }
        
                        if ($result) {
                            $response = $this->successResponse('DELETE', 'Employee Data Deleted Successfully', $result);
                            echo json_encode($response);
                        } else {
                            $response = $this->failedResponse('DELETE', 'Failed to Delete Employee Data', $result);
                            echo json_encode($response);
                        }
                    } else {
                        $response = $this->failedResponse('DELETE', 'failed', 'ID does not match the payload');
                        echo json_encode($response);
                    }
                } else {
                    $response = $this->failedResponse('DELETE', 'failed', 'ID is missing');
                    echo json_encode($response);
                }
            } else {
                $response = $this->failedResponse('DELETE', 'failed', 'ID is null');
                echo json_encode($response);
            }
        
            return $flag; 
        }
        
    
    }


    $request_method = $_SERVER['REQUEST_METHOD'];

    $received_data = [];
    
    if ($request_method === 'PUT' || $request_method === 'DELETE') {
        $request_uri = $_SERVER['REQUEST_URI'];
        $exploded_request_uri = array_values(explode("/", $request_uri));
    
        $last_index = count($exploded_request_uri) - 1;
        $ids = $exploded_request_uri[$last_index];
        $received_data['id'] = $ids;
    }
    
    if ($request_method === 'GET' || $request_method === 'POST' || $request_method === 'PUT' || $request_method === 'DELETE') {
        $input_data = json_decode(file_get_contents('php://input'), true);
        if (is_array($input_data)) {
            $received_data = array_merge($received_data, $input_data);
        }
    }
    

    $api = new API;

    switch ($request_method) {
        case 'GET':
            $api->httpGet($received_data);
            break;
        case 'POST':
            $api->httpPost($received_data);
            break;
        case 'PUT':
            $api->httpPut($ids, $received_data);
            break;
        case 'DELETE':
            $api->httpDelete($ids, $received_data);
            break;
    }
?>
